import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

  const Stack = createStackNavigator()

import LoginScreen from './screens/LoginScreen'
import SignupScreen from './screens/SignupScreen'
import HomeScreen from './screens/HomeScreen'
import UsersList from './screens/UsersList'
import CreateUserScreen from './screens/CreateUserScreen'
import UserDetailScreen from './screens/UserDetailScreen'

function MyStack() {
  return(
    <Stack.Navigator>
      <Stack.Screen 
        name="LoginScreen" 
        component={LoginScreen} 
        options={{title: 'Login'}} 
      />
      <Stack.Screen 
        name="SignupScreen" 
        component={SignupScreen} 
        options={{title: 'Crear Cuenta'}} 
      />
      <Stack.Screen 
        name="HomeScreen" 
        component={HomeScreen} 
        options={{title: 'Home'}} 
      />
      <Stack.Screen 
        name="UsersList" 
        component={UsersList} 
        options={{title: 'Lista de usuarios'}} 
      />
      <Stack.Screen 
        name="CreateUserScreen" 
        component={CreateUserScreen} 
        options={{title: 'Crear un nuevo usuario'}}
      />
      <Stack.Screen 
        name="UserDetailScreen" 
        component={UserDetailScreen} 
        options={{title: 'Detalles de usuarios'}}
      />
    </Stack.Navigator>
  )
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
