import firebase from 'firebase'

import 'firebase/firestore'

var firebaseConfig = {
    apiKey: "AIzaSyBzc2Pmq70rpMRG6clo-WYjhf-KWS4xxUw",
    authDomain: "react-native-firebase-ex-ad0f9.firebaseapp.com",
    projectId: "react-native-firebase-ex-ad0f9",
    storageBucket: "react-native-firebase-ex-ad0f9.appspot.com",
    messagingSenderId: "1060260751079",
    appId: "1:1060260751079:web:adedab01a8d0a243992442"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db = firebase.firestore()

  export default {
    firebase,
    db,
  }
