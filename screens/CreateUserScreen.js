import React, {useState} from 'react'
import {View, Button, TextInput, ScrollView, StyleSheet} from 'react-native'
import firebase from '../database/firebase'

const CreateUserScreen = (props) => {

    const [state, setState] = useState({
        nombre: '',
        email: '',
        telefono: ''
    });

    const handleChangeText = (nombre, value) => {
        setState({ ...state, [nombre]: value})
    };

    const saveNewUser = async () => {
        if(state.nombre === '') {
            alert('Porfavor ingresar su nombre')
        } else {
            try{
                await firebase.db.collection('usuarios').add({
                    nombre: state.nombre,
                    email: state.email,
                    telefono: state.telefono
                })
                props.navigation.navigate('UsersList');
            } catch (error) {
                console.log(error);
            }
        }
    };

    return (
        <ScrollView style={styles.container}>
            <View style={styles.inputGroup}>
                <TextInput placeholder="Nombre del usuario" onChangeText={(value) => handleChangeText('nombre', value)}/>
            </View>
            <View style={styles.inputGroup}>
                <TextInput placeholder="Email del usuario" onChangeText={(value) => handleChangeText('email', value)}/>
            </View>
            <View style={styles.inputGroup}>
                <TextInput placeholder="Telefono del usuario" onChangeText={(value) => handleChangeText('telefono', value)}/>
            </View>
            <View>
                <Button title="Guardar Usuario" onPress={() => saveNewUser()}/>
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 35
    },  
    inputGroup: {
        flex: 1,
        padding: 0, 
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc'
    }
})

export default CreateUserScreen