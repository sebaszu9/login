import React, {useContext, useState} from 'react';
import {View, Text, TouchableOpacity, Platform, StyleSheet, Alert} from 'react-native';
import * as Facebook from 'expo-facebook';
import * as Google from 'expo-google-app-auth';
import * as AppleAuthentication from 'expo-apple-authentication';
import FormInput from '../components/FormInput';
import FormButton from '../components/FormButton';
import SocialButton from '../components/SocialButton';
import firebase from '../database/firebase'

const SignupScreen = ({navigation}) => {
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [confirmPassword, setConfirmPassword] = useState();
  //const [name, setName] = useState();
  //const [urlImage, setUrlImage] = useState();
  //const [id, setId] = useState();

  //const {register} = useContext(AuthContext);

  async function loginFacebook() {
    try {
      await Facebook.initializeAsync({
        appId: '523043445796796',
      });
      const {
        type,
        token,
      } = await Facebook.logInWithReadPermissionsAsync({
        permissions: ['public_profile', 'email'],
      });
      if (type === 'success') {
        // Get the user's name using Facebook's Graph API
        //const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
        const response = await fetch(`https://graph.facebook.com/me?fields=id,name,picture.type(large),email&access_token=${token}`);
        /*await firebase.db.collection('usuarios').add({
          nombre: state.nombre,
          email: state.email,
          token: state.token,
          urlImage: state.urlImage
        })*/
        const data = await response.json();
        hola(data);
        //Alert.alert('Logged in!', `Hi ${(await response.json()).name}!`);
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

  async function hola(dat) {
    try{
      await firebase.db.collection('usuarios').add({
        nombre: dat.name,
        email: dat.email,
        id: dat.id,
        picture: dat.picture,
      })
    } catch(error) {
      console.log(error);
    }
  }

  function componentDidMount() {
    this.checkIfLoggedIn();
  }

  /*checkIfLoggedIn = () => {
    firebase.auth().onAuthStateChanged(function(user){
      if(user){
        this.props.navigation.navigate('SignupScreen');
      } else {
        this.props.navigation.navigate('LoginScreen');
      }
    }.bind(this)
    );
  };*/
  
  async function signInWithGoogleAsync() {
    try {
      const result = await Google.logInAsync({
        androidClientId: '1060260751079-1as735kqk7beogb730ias05dam7rk4kk.apps.googleusercontent.com',
        //iosClientId: '1060260751079-1as735kqk7beogb730ias05dam7rk4kk.apps.googleusercontent.com',
        scopes: ['profile', 'email'],
      });
  
      if (result.type === 'success') {
        console.log(result)
        this.onSignIn(result);
        return result.accessToken;
      } else {
        return { cancelled: true };
      }
    } catch (e) {
      return { error: true };
    }
  }

  async function googleLogin(dat) {
    try{
      await firebase.db.collection('usuarios').add({
        nombre: dat.name,
        email: dat.email,
        id: dat.id,
      })
    } catch(error) {
      console.log(error);
    }
  }

  function loginiOS() {
    return (
      <AppleAuthentication.AppleAuthenticationButton
        buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
        buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
        cornerRadius={5}
        style={{ width: 200, height: 44 }}
        onPress={async () => {
          try {
            const credential = await AppleAuthentication.signInAsync({
              requestedScopes: [
                AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
                AppleAuthentication.AppleAuthenticationScope.EMAIL,
              ],
            });
            // signed in
          } catch (e) {
            if (e.code === 'ERR_CANCELED') {
              // handle that the user canceled the sign-in flow
            } else {
              // handle other errors
            }
          }
        }}
      />
    );
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>Crear una cuenta</Text>

      <FormInput
        labelValue={email}
        onChangeText={(userEmail) => setEmail(userEmail)}
        placeholderText="Email"
        iconType="user"
        keyboardType="email-address"
        autoCapitalize="none"
        autoCorrect={false}
      />

      <FormInput
        labelValue={password}
        onChangeText={(userPassword) => setPassword(userPassword)}
        placeholderText="Contraseña"
        iconType="lock"
        secureTextEntry={true}
      />

      <FormInput
        labelValue={confirmPassword}
        onChangeText={(userPassword) => setConfirmPassword(userPassword)}
        placeholderText="Confirmar contraseña"
        iconType="lock"
        secureTextEntry={true}
      />

      <FormButton
        buttonTitle="Crear Cuenta"
        onPress={() => SignupScreen(email, password)}
      />

      <View style={styles.textPrivate}>
        <Text style={styles.color_textPrivate}>
          Al registrarse acepta nuestros{' '}
        </Text>
        <TouchableOpacity onPress={() => alert('Terms Clicked!')}>
          <Text style={[styles.color_textPrivate, {color: '#e88832'}]}>
            Terminos y condiciones
          </Text>
        </TouchableOpacity>
        <Text style={styles.color_textPrivate}> y </Text>
        <Text style={[styles.color_textPrivate, {color: '#e88832'}]}>
          Privacidad
        </Text>
      </View>

      {Platform.OS === 'android' ? (
        <View>
          <SocialButton
            buttonTitle="Registrate con Facebook"
            btnType="facebook"
            color="#4867aa"
            backgroundColor="#e6eaf4"
            onPress={() => loginFacebook()}
          />
    
          <SocialButton
            buttonTitle="Registrate con Google"
            btnType="google"
            color="#de4d41"
            backgroundColor="#f5e7ea"
            onPress={() => signInWithGoogleAsync()}
          />

          <SocialButton
            buttonTitle="Registrate con iOS"
            btnType="apple"
            color="#ffffff"
            backgroundColor="#000000"
            onPress={() => loginiOS()}
          />
        </View>
      ) : null}

      <TouchableOpacity
        style={styles.navButton}
        onPress={() => navigation.navigate('LoginScreen')}>
        <Text style={styles.navButtonText}>Ya tienes una cuenta, inicia sesion</Text>
      </TouchableOpacity>
    </View>
  );
};

export default SignupScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f9fafd',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 20,
  },
  text: {
    fontSize: 28,
    marginBottom: 10,
    color: '#051d5f',
  },
  navButton: {
    marginTop: 15,
  },
  navButtonText: {
    fontSize: 18,
    fontWeight: '500',
    color: '#2e64e5',
  },
  textPrivate: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 35,
    justifyContent: 'center',
  },
  color_textPrivate: {
    fontSize: 13,
    fontWeight: '400',
    color: 'grey',
  },
});