import React, {useEffect, useState} from 'react'
import {View, Text, ScrollView, Button} from 'react-native'
import firebase from '../database/firebase'
import {ListItem, Avatar} from 'react-native-elements'

const UsersList = (props) => {

    const [users, setUsers] = useState([])

    useEffect(() => {
        firebase.db.collection('usuarios').onSnapshot(querySnapshot =>  {

            const users = [];

            querySnapshot.docs.forEach(doc => {
                const {nombre, email, telefono} = doc.data()
                users.push({
                    id: doc.id,
                    nombre, 
                    email, 
                    telefono
                })
            });

            setUsers(users)
        });
    }, []);

    return (
        <ScrollView>
            <Button 
                title="Crear usuarios" 
                onPress={() => props.navigation.navigate('CreateUserScreen')}
            />

            {
                users.map(user => {
                    return (
                        <ListItem key={user.id} bottomDivider onPress={() => {
                            props.navigation.navigate('UserDetailScreen', {
                                userId: user.id
                            })
                        }}>
                        <ListItem.Chevron/>
                        <Avatar 
                            source={{
                                uri: 
                                    "https://i.pinimg.com/originals/0c/3b/3a/0c3b3adb1a7530892e55ef36d3be6cb8.png",
                            }}
                            rounded
                        />
                        <ListItem.Content>
                            <ListItem.Title>{user.nombre}</ListItem.Title>
                            <ListItem.Subtitle>{user.email}</ListItem.Subtitle>
                        </ListItem.Content>
                        </ListItem>

                    )
                })
            }
        </ScrollView>
    )
}

export default UsersList