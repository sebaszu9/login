import React, {useEffect, useState} from 'react'
import { ActivityIndicator } from 'react-native';
import {View, StyleSheet, TextInput, ScrollView, Button, Alert} from 'react-native'
import { State } from 'react-native-gesture-handler';
import firebase from '../database/firebase'

const UserDetailScreen = (props) => {

    const initialState = {
        id: '',
        nombre: '',
        email: '',
        telefono: ''
    }

    const [user, setUser] = useState(initialState);
    const [loading, setLoading] = useState(true)

    const getUserById = async (id) => {
        const dbRef = firebase.db.collection('usuarios').doc(id)
        const doc = await dbRef.get();
        const user = doc.data();
        setUser({
            ...user,
            id: doc.id,
        });
        setLoading(false)
    };
    
    useEffect(() => {
        getUserById(props.route.params.userId);
    }, []);

    const handleChangeText = (nombre, value) => {
        setUser({ ...user, [nombre]: value})
    };

    const deleteUser = async () =>{
        const dbRef =  firebase.db.collection('usuarios').doc(props.route.params.userId);
        await dbRef.delete();
        props.navigation.navigate('UsersList')
    };

    const updateUser = async () => {
        const dbRef = firebase.db.collection('usuarios').doc(user.id);
        await dbRef.set({
            nombre: user.nombre,
            email: user.email,
            telefono: user.telefono
        })
        setUser(initialState)
        props.navigation.navigate('UsersList')
    };

    const openConfirmationAlert = () => {
        Alert.alert('Eliminar usuario', 'Estas seguro de eliminar este usuario?', [
            {text: 'Si', onPress: () => deleteUser()},
            {text: 'No', onPress: () => console.log(false)}
        ])
    }

    if(loading){
        return(
            <View>
                <ActivityIndicator size="large" color="#9e9e9e" />
            </View>
        )
    }

    return (
        <ScrollView style={styles.container}>
        <View style={styles.inputGroup}>
            <TextInput 
                placeholder="Nombre del usuario" 
                value={user.nombre}
                onChangeText={(value) => handleChangeText('nombre', value)}
            />
        </View>
        <View style={styles.inputGroup}>
            <TextInput 
                placeholder="Email del usuario" 
                value={user.email}
                onChangeText={(value) => handleChangeText('email', value)}
            />
        </View>
        <View style={styles.inputGroup}>
            <TextInput 
                placeholder="Telefono del usuario" 
                value={user.telefono}
                onChangeText={(value) => handleChangeText('telefono', value)}
            />
        </View>
        <View>
            <Button color="#19ab38" title="Actualizar Usuario" onPress={() => updateUser()}/>
        </View>
        <View>
            <Button color="#ff0000" title="Eliminar Usuario" onPress={() => openConfirmationAlert()}/>
        </View>
    </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 35
    },
    inputGroup: {
        flex: 1,
        padding: 0, 
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc'
    }
});

export default UserDetailScreen